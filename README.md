Android application to pull NTP server information from the DHCP server being 
used by the device. The client should always prefer the NTP servers used by DHCP
unless DHCP is not working, in which case it will fallback to an NTP server 
specified by the user.

Requires root. 